function fibo(aug: Array<number>,current: number,max: number): Array<number> {
    const len: number = aug.length
    const last: number = aug[len - 1]
    let next: number = 1
    if(len < max) {
       
        if(current === 1){
           aug.push(1)
           aug.push(1)
           next = 2
           fibo(aug,next,max)
        } else {
           aug.push(current)
           next = last + current 
           fibo(aug,next,max)
        } 

    }
    return aug
}

function clearance(result: Array<number>): string {
    const data: Array<number> = []
    for(let i = 0; i < (result.length + 1); i++){
         const k = result.length - i
         if(result[k] % 2 == 0){} else {
            data.push(result[k])
         }
         
    }
    const results = data.toString()
    return results.substring(1)
}

const result = fibo([],1,4) 
console.log(`deret fibonaci ${result}`)
console.log('deret terbalik tanpa bil genap ' + clearance(result))

const result2 = fibo([],1,6) 
console.log(`deret fibonaci ${result2}`)
console.log('deret terbalik tanpa bil genap ' + clearance(result2))

const result3 = fibo([],1,8) 
console.log(`deret fibonaci ${result3}`)
console.log('deret terbalik tanpa bil genap ' + clearance(result3))

function maxlen(input: string) {
    const check: string = 'abcdefghijklmnopqrstuvwxyz'
    const len: number = check.length
    let ret: number = 0
    let char: string = ''
    let j = 2
    while(j < 8){
 let step: number = Math.ceil(len/j)
    for(let i = 0; i < j; i++){
        const start: number = i * step
        const next: number = i === 0 ? step : step + (i * step)
        const subst: string = check.substring(start,next)
        const re = new RegExp(subst,'gi')
        const status: boolean = re.test(input) 
        if(status){
            ret = subst.length
            char = ' karakter= ' + subst
            break;
        }
    }
    if(ret !== 0){
        break
        } 
    j++
    }
   return ret + char 
}

console.log(maxlen('fsfsahfashfghijklmnasf'))
console.log(maxlen('jdhgknzdfkadfjeijvnskabcde'))